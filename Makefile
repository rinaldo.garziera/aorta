# Rinaldo Garziera  20220902 
#sub--makefile


FIGURES=\
results


all: Makefile $(FIGURES) 
	@set -e ;\
	for i in $(SUBDIRS); \
	do $(MAKE) -C  $$i ; \
	done
clean: 
	-rm *.dat *.bez *.xy *.gpi results *.pdf
	-rm c b 
	-rm -fr $(FIGURES)
	@set -e ;\
	for i in $(SUBDIRS); \
	do $(MAKE) clean  -C  $$i ; \
	done

b: Makefile b.c	
	gcc -Wall -o b  b.c -lm
c: Makefile c.c	
	gcc -Wall -o c  c.c -lm

results: analysis.sh b c  Makefile .specimen.gpi *.svg
	./analysis.sh

tgz:
	-tar --directory ..  --exclude=.git --exclude=tmp --exclude=tar.Z  -cvf - `pwd -P | sed '-e s/\/.*\///g'` | gzip -c > tar.Z/`pwd -P | sed '-e s/\/.*\///g'`.`date | sed '-e s/ /_/g' | sed '-e s/:/_/g'`.tar.gz
	-rclone sync tar.Z yandex:lavori/h
