// Rinaldo Garziera 20220902 
//curva di bezier con 4 punti di controllo
//./b step < control.points > curve.xy
//control points
//a b
//c d //moveto e lineto del segmento da 50 mm
//x0 y0
//x1 y1 x2 y2 x3 y3
//x4 y4 x5 y5 x6 y6
//...
//file.xy (punti della curva)
//a b
//c d
//x1 y1
//x2 y2
//...

#include <stdio.h>
#include <math.h>
#include<stdlib.h>

int main(int argc,char **argv)
{
	double junk,step;
	double x[4];
	double y[4];
	double a,b,c,d;
	double *X,*Y;
	int count=0,NP; //numero righe da leggere; numero punti nell'intervallo 0-1
	if(argc!=2){fprintf(stderr,"too many or too few arguments\n"); exit(0);}
	NP= atoi(argv[1]);

	fscanf(stdin,"%lf %lf", &junk,&junk);count++; //prima riga
	fscanf(stdin,"%lf %lf", &junk,&junk);count++; //seconda riga
	fscanf(stdin,"%lf %lf", &junk,&junk);count++; //terza riga(moveto di bezier)
	while(fscanf(stdin,"%lf %lf %lf %lf %lf %lf",&junk, &junk, &junk,&junk,&junk,&junk)!=EOF) count++;
fprintf(stderr,"count= %d\n",count);
	int L=(count-3)*3; //punti di bezier(va aggiunto il primo)
	X=calloc(L+1,sizeof(double)); 
	Y=calloc(L+1,sizeof(double)); 
	rewind(stdin);
	fscanf(stdin,"%lf %lf", &a,&b); //prima riga
	fscanf(stdin,"%lf %lf", &c,&d); //seconda riga
	double scale=sqrt((a-c)*(a-c)+(b-d)*(b-d))/50; //segmento sempre di
//lunghezza 50 mm
	fprintf(stdout,"%15.14e %15.14e\n%15.14e %15.14e\n", a/scale,b/scale,c/scale,d/scale);
	fscanf(stdin,"%lf %lf", X,Y); //terza riga
	int i;
	for(i=0;i<L;i+=3)
		fscanf(stdin,"%lf %lf %lf %lf %lf %lf",X+i+1,Y+i+1,X+i+2,Y+i+2,X+i+3,Y+i+3);
	for(i=0;i<L;i+=3)
	{
		double t,xt,yt;
		step=1.0/NP;
		for (t = 0.0; t < 1.0; t += step)
		{	
			x[0]=X[i]/scale;
			y[0]=Y[i]/scale; 
			x[1]=X[i+1]/scale;
			y[1]=Y[i+1]/scale;
			x[2]=X[i+2]/scale;
			y[2]=Y[i+2]/scale;
			x[3]=X[i+3]/scale;
			y[3]=Y[i+3]/scale;

			xt = pow(1-t,3)*x[0]+3*t*pow(1-t,2)*x[1]+3*pow(t,2)*(1-t)*x[2]+pow(t,3)*x[3];
			yt = pow(1-t,3)*y[0]+3*t*pow(1-t,2)*y[1]+3*pow(t,2)*(1-t)*y[2]+pow(t,3)*y[3];
			fprintf(stdout, "%15.14e %15.14e\n",xt,yt);
		}
	}
}	


