// Rinaldo Garziera 20220802  20240110
//legge un file di punti x y e determina: lunghezza curva, rho e altri dati
//./c NP CIRC NOMESPEC < file.xy > ./results
//NP numero punti ogni tratto bezier
//CIRC vale 1 se provino circolare altrimenti 0
//NOMESPEC nome del provino
//file.xy
//x0 y0
//x1 y1
//..
//file.dat
//x y s rho d_dro
//x e y sono i punti di x.dat
//s e',l'ascissa curvilinea
//rho e' il raggio di curvatura
//1/rho e' la curvatura

//results e' il file dei risultati cospicui

#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>


int main(int argc,char **argv)
{
	char nomespec[20];
	double *x,*y,a,b,c,d,*xg,*yg;
	double junk;
	double *d_a, *d_s1, *d_s2,*rho,*s;
	double *rhog,*cu,*ce,*sg;
	int count=0;
	int i=0,NP;
	int CIRC;
	double max_cur_eff=0, ang_open;
	double alpha_curl=0;
	if(argc!=4){fprintf(stderr,"too many or too few arguments\n"); exit(0);}
	NP=atoi(argv[1]);
	CIRC=atoi(argv[2]);
	strcpy(nomespec,argv[3]);

        while(fscanf(stdin,"%lf %lf", &junk,&junk)!=EOF) count++;
        count-=2; //le prime due righe contengono a b c d
        rewind(stdin);

	xg=calloc(count,sizeof(double)); //x completa
	yg=calloc(count,sizeof(double)); //y completa
	sg=calloc(count,sizeof(double)); //s completa
	rhog=calloc(count,sizeof(double)); //rho completa
	cu=calloc(count,sizeof(double)); //curvatura
	ce=calloc(count,sizeof(double)); //curvatura effettiva
	x=calloc(NP,sizeof(double)); //x tratto bezier
	y=calloc(NP,sizeof(double)); //y tratto bezier 
	rho=calloc(NP,sizeof(double)); // raggio di curvatura
	s=calloc(NP,sizeof(double)); // coordinata curvilinea
	d_a=calloc(NP,sizeof(double)); // differenziale dell'angolo
	d_s1=calloc(NP,sizeof(double)); // primo ds
	d_s2=calloc(NP,sizeof(double)); // secondo ds 

//leggo x e y complete (g starebbe pre global)
	i=0;
	fscanf(stdin,"%lf %lf", &junk,&junk); //vado avanti di due righe
	fscanf(stdin,"%lf %lf", &junk,&junk);
	while(fscanf(stdin,"%lf %lf",xg+i,yg+i)!=EOF) i++;
	rewind(stdin);

	fscanf(stdin,"%lf %lf", &a,&b);// estremi segmento di misura (5cm)
	fscanf(stdin,"%lf %lf", &c,&d);
//trovo il punto della curva piu' vicino a c,d (o a,b)(estremo segmento)
	double dist=1e10; //parto da distanza siderale
	int II=0;
	double dd1,dd2,dd;
	for(i=0;i<count;i++)
	{
		dd1=sqrt((a-xg[i])*(a-xg[i])+(b-yg[i])*(b-yg[i]));
		dd2=sqrt((c-xg[i])*(c-xg[i])+(d-yg[i])*(d-yg[i]));
		if(dd1<dd2)dd=dd1;else dd=dd2;
		if(dist>dd){dist=dd;II=i;}
	}	
//qual'e' il vertice del segmento piu' vicino alla curva?
	double vx,vy;
//qual'e' il vertice del segmento piu' lontano dlla curva?
	double Vx,Vy;
	dd1=sqrt((a-xg[II])*(a-xg[II])+(b-yg[II])*(b-yg[II]));
	dd2=sqrt((c-xg[II])*(c-xg[II])+(d-yg[II])*(d-yg[II]));
	if(dd1>dd2){vx=c;vy=d;Vx=a;Vy=b;}else{vx=a;vy=b;Vx=c;Vy=d;}
//	double LSEG;
//	LSEG=sqrt((vx-Vx)*(vx-Vx)+(vy-Vy)*(vy-Vy));
/////////////
        int SGN; //segno globale della curvatura

//           *
//          *  SGN = -1 (super apertura)
//        0 * __________________
//           *
//             *

//         *
//          *  SGN = 1 (apertura minore 180)
//        0 * __________________
//         *
//        *

//Il pirolino, 0, e' sempre all interno dell'arteria


//prodotto scalare vettore tangente alla curva - segmento
	double ex,ey,dx1,dy1,dx2,dy2,ps1,ps2;
//componenti vettore dal punto II (piu' vicino al punto piu'vicino del segmento
        ex= Vx-vx;
        ey= Vy-vy;

        dx1= xg[0]-xg[II];
        dy1= yg[0]-yg[II];
        dx2= xg[count-1]-xg[II];
        dy2= yg[count-1]-yg[II];
//prodotto scalare e segno
        ps1=(dx1*ex+dy1*ey)/(sqrt(dx1*dx1+dy1*dy1)*sqrt(ex*ex+ey*ey));
        ps2=(dx2*ex+dy2*ey)/(sqrt(dx2*dx2+dy2*dy2)*sqrt(ex*ex+ey*ey));
        SGN=ps1+ps2<0?1:-1; //SGN positivo se si apre meno di 180 come da figura sopra

	double LUNG=0;
	for(i=0;i<count-1;i++)
	{
		LUNG+=sqrt((xg[i+1]-xg[i])*(xg[i+1]-xg[i])+\
		          ((yg[i+1]-yg[i])*(yg[i+1]-yg[i])));
	}
//valuto se la convessita' e' verso l'esterno o verso l'interno
//facendo la media delle congiungenti i punti della curva col
//vertice remoto del segmento e confrontando con la lunghezza
//del segmento stesso
	double dist_med=0;
	for(i=0;i<count-1;i++)
	{
		dist_med+=sqrt((xg[i]-Vx)*(xg[i]-Vx)+\
			   (yg[i]-Vy)*(yg[i]-Vy));
	}
	dist_med/=count;
//il segment di riferimento e' interno o esterno?
//       	int EXT_INT; //segno globale della curvatura
//	EXT_INT=dist_med>LSEG?1:0;
	double R;//raggio supposto del provino in vivo
	R=1e10;

//inizio analisi tratti bezier
	int k; double S=0;
	int N_BEZ_SETS=count/NP;
	double xxx,yyy;
	for(k=0;k<N_BEZ_SETS;k++)
	{
		for(i=0;i<NP;i++)
		{
			fscanf(stdin,"%lf %lf",&xxx,&yyy);
			x[i]=xxx;y[i]=yyy;
		}

		
//curvatura
		s[0]=S;
		for(i=0;i<NP-2;i++)
		{
			double ds1,ds2,da;
                	ds1=sqrt(\
                	pow((x[i+1]-x[i]),2)+\
                	pow((y[i+1]-y[i]),2));

                	ds2=sqrt(\
                	pow((x[i+2]-x[i+1]),2)+\
                	pow((y[i+2]-y[i+1]),2));
			double PPS= (\
                	(((x[i+2]-x[i+1])*(x[i+1]-x[i]))+\
                	((y[i+2]-y[i+1])*(y[i+1]-y[i])))/\
                	(ds1*ds2)
                	);
			if(fabs(PPS)>.9999999999)PPS=.9999999999;
                	da=acos(PPS);
                	double sgn=1; //segno prodotto vettoriale tra i due ds
                	sgn=((x[i+2]-x[i+1])*(y[i+1]-y[i])-\
                     	(x[i+1]-x[i])*(y[i+2]-y[i+1]))>0?1:-1;
			d_a[i]=da; d_s1[i]=ds1; d_s2[i]=ds2;
               	//	rho[i]=SGN*sgn*ds1/da;                
			alpha_curl+=sgn*da;
               		rho[i]=sgn*ds1/da;                
			s[i+1]=s[i]+ds1;
			S=s[i+1];
			if(rho[i]==0) rho[i]=1e-3;

		}
		if(CIRC) R=LUNG/(2*M_PI);
	
		for(i=0;i<NP-2;i++)
		{
			fprintf(stdout, "%15.14e %15.14e %15.14e %15.14e %15.14e %15.14e\n" ,x[i],y[i],s[i],rho[i],1/rho[i],1/rho[i]-1/R);
			sg[i+NP*k]=s[i];
			rhog[i+NP*k]=rho[i];
			cu[i+NP*k]=1/rho[i];
			ce[i+NP*k]=SGN*fabs(1/rho[i])-1/R;	
		}
	}	
	double ce_med=0;
	for(i=0;i<(NP-1)*k;i++)
	{
		ce_med+=ce[i];
		if(fabs(ce[i])>fabs(max_cur_eff))
		{
			max_cur_eff=ce[i];
		}	
	}
	ce_med=ce_med/((NP-1)*k);

//effettivo angolo di circolazione. Cioe' tenendo conto, come per la curvatura effettiva, della naturale forma del provino prima di essere tagliato. Ha senso solamente per le  circolari
	double alpha_c;

	if(CIRC)
	{
		if(SGN==1)
		{
			alpha_c=(2*M_PI-fabs(alpha_curl))/2;
		}
		else
		{
			alpha_c= (2*M_PI+fabs(alpha_curl))/2;
		}
	}
	else alpha_curl*= -SGN; //convenzionale, positivo se la generatrice si apre verso l'esterno

	if(CIRC)
        {
		double ps,l1,l2;
		ps=((-xg[II]+xg[0])*(-xg[II]+xg[count-1])+      \
                    (-yg[II]+yg[0])*(-yg[II]+yg[count-1]));
             	l1=sqrt((xg[II]-xg[0])*(xg[II]-xg[0])+\
                	(yg[II]-yg[0])*(yg[II]-yg[0]));
                l2=sqrt((xg[II]-xg[count-1])*(xg[II]-xg[count-1])+\
                	(yg[II]-yg[count-1])*(yg[II]-yg[count-1]));
                ang_open=SGN>0?acos(ps/(l1*l2)):-acos(ps/(l1*l2))+2*M_PI;
//old style angle

//		fprintf(stderr,"%s	%15.14e	%15.14e	%15.14e	%15.14e	%15.14e	%15.14e	%i\n",\
			 nomespec, LUNG, R, -1/R, ce_med, \
			ang_open*180/M_PI,alpha_c*180/M_PI,SGN);
		fprintf(stderr,"%s	%15.14e	%15.14e	%15.14e	%i\n",\
			 nomespec, ce_med, \
			ang_open*180/M_PI,alpha_c*180/M_PI,SGN);
			//ang_open*180/M_PI,alpha_c*180/M_PI,SGN,alpha_curl*180/M_PI);
	}
	else
	{
	char not_app[]="\t\t n.a.";
//		fprintf(stderr,"%s	%15.14e	%15.14e	%15.14e	%15.14e	%s	%15.14e	%i\n",\
			 nomespec, LUNG, R, -1/R, ce_med, \
			not_app,alpha_curl*180/M_PI,SGN);
		fprintf(stderr,"%s	%15.14e	%s	%15.14e	%i\n",\
			 nomespec,  ce_med, \
			not_app,alpha_curl*180/M_PI,SGN);
	}
}
