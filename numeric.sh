#!/bin/bash
set -x
cd /home/garz/h
make clean
#rm -fr aorta.numeric
cp -dpR /home/garz/h /home/garz/aorta.numeric
#
cd /home/garz/aorta.numeric
#list=$(find . -type f | grep -E "\.sty$|\.tex$|\.pdf$|\.jpeg$|\.jpg$|\.eps") 
#rm -fr $list
#list=$(find . -type d | grep -E "old")
#rm -fr $list
#rm githubtoken numeric.sh overleaf.sh *.JPG
rm  numeric.sh 
rm -fr .git/*
cp /home/garz/h/.git/config /home/garz/aorta.numeric/.git/config
cp /home/garz/h/.gitignore /home/garz/aorta.numeric/.gitignore

echo '
[remote "numeric"]
        url = git@gitlab.com:rinaldo.garziera/aorta.numeric.git
        fetch = +refs/heads/*:refs/remotes/origin/*
'>> /home/garz/aorta.numeric/.git/config
git init 
git add .
date=`date`
echo "software for aorta slices curvature analysis $date" | git commit -F - .
git push -f numeric
cd
rm -fr aorta.numeric
