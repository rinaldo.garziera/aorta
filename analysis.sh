#!/bin/sh
set -x
rm results
echo "specimen		CUR_EFF_MED	    OLD_STY_ANGLE	     NEW_ANGLE			GEOM_CURL      SGN		LUNG" > ./results
FILES=`ls *.svg`
for i in $FILES; do
	FILE=`echo $i | sed -e "s/\.svg//"`
	#estrazione punti bezier
	cat $FILE.svg | sed -n '/<path.*L/{p;q}' | sed -e "s/^.*M//" | sed -e "s/\".*$//" \
	| sed -e "s/L/\\n/g" > $FILE.bez
	cat $FILE.svg | sed -n '/<path.*C/p' | sed -e "s/^.*M//" | sed -e "s/\".*$//" \
	| sed -e "s/L.*C/C/"\
	| sed -e "s/C/\\n/g" >> $FILE.bez
	#set punti xy bezier
	NP=1000
	NOT_L=`echo $FILE | sed -e "/c/d"`

	#di che tipo di provino si tratta?
	if [[ $NOT_L == '' ]]
 	then
		CIRC=1
	else
		CIRC=0
	fi
	echo $CIRC

	./b $NP < $FILE.bez > $FILE.xy
	#analisi curvatura
	./c $NP $CIRC $FILE < $FILE.xy > $FILE.dat 2>> ./results 
	cat ./.specimen.gpi | sed -e "s/specimen/$FILE/" > $FILE.gpi
	#plot risultati
	gnuplot $FILE.gpi
done
cat results | awk -v OFS='\t' '{ print $1,     $2,     $3,     $4 }' > Results
cat Results | sed -e "/_l/d" >Results_circular
cat Results | sed -e "/_c/d" >Results_linear
cat Results | sed -e "s/[\t]\+/\;/g" | sed -e "s/\ //g" > Results.csv
